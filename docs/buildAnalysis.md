<p align="center">
  <h1 align="center">镜像分析工具</h1>
</p>

LiteOS Studio对构建出的elf文件进行内存占用分析，支持LiteOS开发者快速评估内存段、符号表使用情况。


### 1、镜像分析页面入口
> 打开Liteos Studio工具，点击调测工具里的镜像分析tab页签，设置编译器、可执行文件路径等，然后点击确定进入镜像分析页面。

![avatar](images/buildAnalysis/buildAnalysisStart.png)

### 2、镜像分析相关配置
- 镜像分析依赖工程的elf文件、map文件、编译器类型、编译器路径，需在调测工具设置界面进行相关设置，如下图所示，其中红色边框的为必选项。路径中不要带中文，空格和特殊字符。

![avatar](images/buildAnalysis/buildAnalysisConfig.png)

注：上图中编译器目录指的是.exe可执行文件所在的路径，例如：如果编译器类型是arm-none-eabi，arm-none-eabi.exe编译器存放路径如下图所示，因此就填写该路径即可。

![avatar](images/buildAnalysis/buildAnalysisConfig0.png)

- 镜像分析支持模块配置，如下图所示，选择对应的json文件，json内容按照如下方式配置。可以配置.a或.o文件属于哪个模块，镜像分析模块大小界面会根据这个配置进行汇总。

![avatar](images/buildAnalysis/buildAnalysisConfig1.png)

- 镜像分析结果展示界面可修改elf文件路径，配置好elf文件路径后，可点击右边的按钮重新执行镜像分析。

![avatar](images/buildAnalysis/buildAnalysisConfig2.png)

- 如果需要在镜像分析结果展示界面使用函数跳转到源码的功能，且工程源码在linux上，可将工程映射到windows磁盘上，然后进行相关路径配置如下图所示。

![avatar](images/debugToolsSet.png)


### 3、镜像分析页面介绍
镜像分析页面分内存区域、详细信息、文件大小和模块大小4个界面。

#### 内存区域界面
内存区域界面以表格的形式展示了每个区域的内存使用情况，包含了起始地址、结束地址、大小、可用内存以及内存占用。
表格下面默认显示内存占比最高的前三个区域内容。点击不同的行会在下面仪表盘中显示点击行的数据。

![avatar](images/buildAnalysis/buildAnalysisRegions.png)

如果列表不足三条数据，则显示的图跟着鼠标选中不同的行而变化。

![avatar](images/buildAnalysis/buildAnalysisRegions1.png)

#### 详细信息界面
详细信息里显示了Section和Symbol的信息。通过树状表格展示层级关系、名称、VMA、LMA以及size。双击Symbol子节点会跳转到具体的代码行号。页面也支持搜索Name定位到具体的行，然后通过键盘Enter键跳转到下一个符合搜索条件的行，可以开启支持正则和区分大小写搜索，搜索结果是符合条件的树形数据。

![avatar](images/buildAnalysis/buildAnalysisDetails.png)

同时，点击表头的每列会对该列数据进行排序。

![avatar](images/buildAnalysis/buildAnalysisDetails1.png)

#### 文件大小界面
文件大小里展示了.a文件和.o文件的层级关系以及不同section的内存占用，点击表头会对该列数据进行排序。

![avatar](images/buildAnalysis/buildAnalysisFile.png)

如果表头section名称显示不完整，可以将鼠标移到表头则会显示完整的名称。

![avatar](images/buildAnalysis/buildAnalysisFile1.png)

鼠标移动到SUM表头，会显示一个求和的图标，点击该图标默认会选中名称含有data和text的列进行求和计算。

![avatar](images/buildAnalysis/buildAnalysisFile2.png)

用户也可以动态设置需要求和的列。  

![avatar](images/buildAnalysis/buildAnalysisFile3.png)

#### 模块大小界面
模块大小里展示了模块和组件的层级关系以及不同section的内存占用，点击表头会对该列数据进行排序。模块划分可能各个平台上有差异，如果有疑问或需求请联系我们。

![avatar](images/buildAnalysis/buildAnalysisModule.png)

类似文件大小，模块大小的列也都是动态变化的。下面展示的饼图显示父节点和子节点的数据情况，如果不存在子节点，就只显示父节点的图；如果存在子节点，左侧图是父节点，右侧图是子节点。如果模块的Rom Size为0，图表中不显示该项，如果都为0不显示图表。

![avatar](images/buildAnalysis/buildAnalysisModule1.png)

#### 导出功能
点击excel导出按钮会将全部页面数据导出，导出的excel文件中，模块大小会根据配置的模块名称进行汇总。如下图所示。
![avatar](images/buildAnalysis/buildAnalysisExport.png)
![avatar](images/buildAnalysis/buildAnalysisExport1.png)

